# SWOT_AEN

SWOT, Réussir sa formation CDA Aen


$\textcolor{green}{\text{OPPORTUNITES}}$   | $\textcolor{red}{\text{MENACES}}$ 
------------- | -------------
-Stage                                  | -Présentation certification
-Réorientation pro                      | -Contexte économique/Politique
-Plus de facilités à trouver un emploi  | -Pas de stage
-Travail en petit groupe                | -Première session formation
$\textcolor{blue}{\text{FORCES}}$   | $\textcolor{yellow}{\text{FAIBLESSES}}$ 
-Expériences langage                    | -Anglais
-Projets personnel                      | -Manque d'expériences prog
-Facilités d'apprentissage                                     | -Difficulté à se conformer à la culture managériale

